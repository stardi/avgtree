﻿using System;

namespace AvgTree.Model.DialogModels
{
    internal class DlgModelChangedEventArgs : EventArgs
    {
        public IDlgModelTreeNode Model;
    }
}
