﻿namespace AvgTree.Model.DialogModels
{
    internal class DlgModelUpdateTreeNode : DlgModelTreeNode
    {
        private bool _staticUpdate;
        public bool DynamicUpdate
        {
            get { return !_staticUpdate; }
            set { _staticUpdate = !value; OnPropertyChanged(); }
        }

        public bool StaticUpdate
        {
            get { return _staticUpdate; }
            set { _staticUpdate = value; OnPropertyChanged(); }
        }
    }
}

