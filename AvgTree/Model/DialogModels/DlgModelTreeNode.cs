﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AvgTree.Model.DialogModels
{
    internal abstract class DlgModelTreeNode : INotifyPropertyChanged, IDlgModelTreeNode
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }

        private double? _point;
        private int? _idParent;
        private Dictionary<int, string> _items;

        public double? Point
        {
            get { return _point; }
            set { _point = value; OnPropertyChanged(); }
        }

        public bool PointHasValue
        {
            get { return Point.HasValue; }
            set
            {
                if (!value)
                {
                    Point = null;
                }
                else if (!Point.HasValue)
                {
                    Point=0;
                }
            }
        }

        public bool PointIsNull
        {
            get { return !Point.HasValue; }
            set
            {
                if (value)
                {
                    Point = null;
                }
                else if (!Point.HasValue)
                {
                    Point = 0;
                }
            }
        }

        public int? IdParent
        {
            get { return _idParent; }
            set { _idParent = value; OnPropertyChanged(); }
        }

        public Dictionary<int, string> Items => _items ?? (_items = new Dictionary<int, string>());

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual void Update(ITreeListNodeModel treeListItemModel)
        {
            Name = treeListItemModel.Name;
            Point = treeListItemModel.Point;
            IdParent = treeListItemModel.IdParent;

            if (TreeListModel.Instance.Nodes.Count >= 100) return;

            Items[-1] = "Root";
            foreach (ITreeListNodeModel row in TreeListModel.Instance.Nodes)
            {
                Items[row.Id] = $"{row.Name} [{row.Id}]";
            }
        }
    }
}

