﻿namespace AvgTree.Model.DialogModels
{
    internal class DlgModelDeleteTreeNode : DlgModelTreeNode
    {
        public bool DeleteChildren { get; set; }

        public override void Update(ITreeListNodeModel treeListItemModel)
        {
            Name = treeListItemModel.Name;
            Point = treeListItemModel.Point;
            IdParent = treeListItemModel.IdParent;
        }
    }
}

