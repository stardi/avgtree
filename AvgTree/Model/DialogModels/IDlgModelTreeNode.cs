﻿namespace AvgTree.Model.DialogModels
{
    /// <summary>
    /// The inherited classes are used for data bindings in tree node handler dialogs
    /// </summary>
    internal interface IDlgModelTreeNode
    {
        /// <summary>
        /// DbColumn: Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// DbColun: Point
        /// </summary>
        double? Point { get; set; }

        /// <summary>
        /// DbColumn: IdParent
        /// </summary>
        int? IdParent { get; set; }

        /// <summary>
        /// Update the values of this class with the ITreeListNodeModel
        /// </summary>
        /// <param name="treeListItemModel"></param>
        void Update(ITreeListNodeModel treeListItemModel);
    }
}
