﻿using System.Collections.Generic;

namespace AvgTree.Model
{
    /// <summary>
    /// Contains the formatted database record
    /// </summary>
    public interface ITreeListNodeModel
    {
        /// <summary>
        /// DbColumn: ID
        /// </summary>
        int Id { get; }

        /// <summary>
        /// DbColumn: ID_PARENT
        /// </summary>
        int? IdParent { get; }

        /// <summary>
        /// DbColumn: ID_PARENT
        /// </summary>
        ITreeListNodeModel Parent { get; set; }

        /// <summary>
        /// DbColumn: NAME
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// DbColumn: POINT
        /// </summary>
        double? Point { get; set; }

        /// <summary>
        /// Calculated summary of descendant Points + Point
        /// </summary>
        double Sum { get; }

        /// <summary>
        /// Amount of all descendants
        /// </summary>
        int DescendantCount { get; }

        /// <summary>
        /// Amount of all descendants which has valid Point value
        /// </summary>
        int DescendantCountWithPoint { get; }

        /// <summary>
        /// The average of this node
        /// </summary>
        double Average { get; }

        /// <summary>
        /// The value to be show in the Point column o the TreeList
        /// </summary>
        double CalculatedValue { get; }

        /// <summary>
        /// Force to recalculate the average. e.g: When the Point or one of the relative is changed
        /// </summary>
        void InvalidateAverage();

        /// <summary>
        /// Returns the direct children.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ITreeListNodeModel> GetChildren();
    }
}
