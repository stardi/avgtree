﻿using System.Collections.Generic;

namespace AvgTree.Model
{
    internal class TreeListModel
    {
        private static TreeListModel _instance;
        public static TreeListModel Instance => _instance ?? (_instance = new TreeListModel());

        public List<ITreeListNodeModel> Nodes { get; }

        public TreeListModel()
        {
            Nodes = new List<ITreeListNodeModel>();
        }
    }
}
