﻿using System;
using AvgTree.Model;

namespace AvgTree.DataAccess
{
    /// <summary>
    /// AvgTreeDataset Accessor
    /// </summary>
    internal interface ITreeDataManager
    {
        /// <summary>
        /// Initial load
        /// </summary>
        void LoadTreeFromDatabase();

        /// <summary>
        /// Create new node in the database
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="name"></param>
        /// <param name="point"></param>
        /// <returns>The model with the generated data</returns>
        ITreeListNodeModel Add(int? parentId, string name, double? point);

        /// <summary>
        /// Update the node in the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="newParentId"></param>
        /// <param name="newName"></param>
        /// <param name="newPoint"></param>
        void Update(ITreeListNodeModel model, int? newParentId, string newName, double? newPoint);

        /// <summary>
        /// Delete node from database. The children'n new parent will be the node's parent
        /// </summary>
        /// <param name="model"></param>
        void Delete(ITreeListNodeModel model);

        /// <summary>
        /// Delete the node and the descendant nodes from the database
        /// </summary>
        /// <param name="model"></param>
        void DeleteWithDescendants(ITreeListNodeModel model);

        /// <summary>
        /// Async databaseIO started
        /// </summary>
        event EventHandler<EventArgs> OnDbChanging;

        /// <summary>
        /// Async database IO finished
        /// </summary>
        event EventHandler<EventArgs> OnDbChanged;

        /// <summary>
        /// Exception occured during processing
        /// </summary>
        event EventHandler<UnhandledExceptionEventArgs> OnException;

    }
}
