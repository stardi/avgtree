﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AvgTree.DataAccess.AvgTreeDataSetTableAdapters;
using AvgTree.Model;

namespace AvgTree.DataAccess
{
    internal class AvgTreeDataManager : ITreeDataManager
    {
        private static AvgTreeDataManager _instance;
        public static ITreeDataManager Instance => _instance ?? (_instance = new AvgTreeDataManager());
        private AvgTreeDataManager() { }

        public event EventHandler<EventArgs> OnDbChanging;
        public event EventHandler<EventArgs> OnDbChanged;
        public event EventHandler<UnhandledExceptionEventArgs> OnException;

        private AvgTreeDataSet _avgTreeDataSet;
        private AvgTreeDataSet AvgTreeDataSet => _avgTreeDataSet ?? (_avgTreeDataSet = new AvgTreeDataSet());

        private IAvgTreeTableAdapter _avgTreeTableAdapter;
        private IAvgTreeTableAdapter AvgTreeTableAdapter => _avgTreeTableAdapter ?? (_avgTreeTableAdapter = new AvgTreeTableAdapter());

        public async void LoadTreeFromDatabase()
        {
            try
            {
                OnDbChanging?.Invoke(this, EventArgs.Empty);
                await Task.Run(() => AvgTreeTableAdapter.Fill(AvgTreeDataSet.AvgTree));
                TreeListModel.Instance.Nodes.AddRange(AvgTreeDataSet.AvgTree);
                OnDbChanged?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                OnException?.Invoke(this, new UnhandledExceptionEventArgs(ex, false));
                await Task.Delay(1000);
                LoadTreeFromDatabase();
            }
        }

        private readonly object _parallelUpdateLock = new object();

        private async void UpdateDbAdapter()
        {
            await UpdateDbAdapterAsync(true);
        }

        private async Task UpdateDbAdapterAsync(bool invokEvents)
        {
            try
            {
                await Task.Run(() =>
                {
                    lock (_parallelUpdateLock)
                    {
                        if (invokEvents) OnDbChanging?.Invoke(this, EventArgs.Empty);
                        try
                        {
                            AvgTreeTableAdapter.Update(AvgTreeDataSet.AvgTree);
                        }
                        finally
                        {
                            if (invokEvents) OnDbChanged?.Invoke(this, EventArgs.Empty);
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                OnException?.Invoke(this, new UnhandledExceptionEventArgs(ex, true));
            }
        }

        public ITreeListNodeModel Add(int? parentId, string name, double? point)
        {
            var parent = AvgTreeDataSet.AvgTree.FindByID(parentId ?? -1);
            var row = _avgTreeDataSet.AvgTree.AddAvgTreeRow(parent, name, point);
            UpdateDbAdapter();
            TreeListModel.Instance.Nodes.Add(row);
            return row;
        }

        public void Update(ITreeListNodeModel model, int? newParentId, string newName, double? newPoint)
        {
            if ((model.IdParent != newParentId) //Is the parentId changed
                && model.Id != newParentId) //The new parentId cannot point to myself
            {
                model.InvalidateAverage();
                var parent = AvgTreeDataSet.AvgTree.FindByID(newParentId ?? -1);

                //The new parent cannot be any of my child
                var theNewParentIsNotMyChild = true;
                var p = parent;
                while (p?.IdParent != null && (theNewParentIsNotMyChild = (p.IdParent != model.Id)))
                {
                    p = p.AvgTreeRowParent;
                }

                if (theNewParentIsNotMyChild)
                {
                    model.Parent = parent;
                }
            }

            model.Name = newName;
            model.Point = newPoint;

            model.InvalidateAverage();
            UpdateDbAdapter();
        }

        public async void Delete(ITreeListNodeModel model)
        {
            model.Parent?.InvalidateAverage();
            foreach (var child in model.GetChildren())
            {
                child.Parent = model.Parent;
            }
            OnDbChanging?.Invoke(this, EventArgs.Empty);
            try
            {
                await UpdateDbAdapterAsync(false);
                (model as AvgTreeDataSet.AvgTreeRow)?.Delete();
                TreeListModel.Instance.Nodes.Remove(model);
                await UpdateDbAdapterAsync(false);
            }
            finally
            {
                OnDbChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public async void DeleteWithDescendants(ITreeListNodeModel model)
        {
            model.Parent?.InvalidateAverage();
            OnDbChanging?.Invoke(this, EventArgs.Empty);
            try
            {
                await DeleteBranchAsync(model);
            }
            finally
            {
                OnDbChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public async Task DeleteBranchAsync(ITreeListNodeModel model)
        {
            foreach (var child in model.GetChildren())
            {
                await DeleteBranchAsync(child);
            }

            (model as AvgTreeDataSet.AvgTreeRow)?.Delete();
            await UpdateDbAdapterAsync(false);
            TreeListModel.Instance.Nodes.Remove(model);
        }
    }
}
