﻿namespace AvgTree.DataAccess
{
    /// <summary>
    /// The database accessors must implement this interface
    /// </summary>
    internal interface IAvgTreeTableAdapter
    {
        /// <summary>
        /// fill datatable from the database
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        int Fill(AvgTreeDataSet.AvgTreeDataTable dataTable);

        /// <summary>
        /// write dataTable changes into the db
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        int Update(AvgTreeDataSet.AvgTreeDataTable dataTable);
    }
}
