﻿using System.Collections.Generic;
using System.Linq;
using AvgTree.Model;

namespace AvgTree.DataAccess
{
    partial class AvgTreeDataSet
    {
        partial class AvgTreeRow : ITreeListNodeModel
        {
            public int Id => ID;
            public string Name
            {
                get { return NAME; }
                set { NAME = value; }
            }

            public double CalculatedValue => Point ?? Average;

            public int? IdParent
            {
                get
                {
                    return AvgTreeRowParent?.Id;
                }
                set
                {
                    Parent = tableAvgTree.FindByID(value ?? -1);
                }
            }

            public ITreeListNodeModel Parent
            {
                get { return AvgTreeRowParent; }
                set { AvgTreeRowParent = value as AvgTreeRow; }
            }

            public double? Point
            {
                get
                {
                    if (IsPOINTNull()) return null;
                    return POINT;
                }
                set
                {
                    if (value.HasValue)
                    {
                        POINT = value.Value;
                    }
                    else
                    {
                        SetPOINTNull();
                    }
                }
            }


            private double _sum = double.NaN;
            public int DescendantCount { get; private set; }
            public int DescendantCountWithPoint { get; private set; }

            public double Sum
            {
                get { return _sum = double.IsNaN(_sum) ? CalculateSum() : _sum; }
            }

            public void InvalidateAverage()
            {
                _sum = double.NaN;
                DescendantCount = 0;
                DescendantCountWithPoint = 0;
                AvgTreeRowParent?.InvalidateAverage();
            }

            private double CalculateSum()
            {
                var children = GetAvgTreeRows();
                var sum = children.Sum(child => child.CalculateSum());
                DescendantCount = children.Length + children.Sum(child => child.DescendantCount);
                DescendantCountWithPoint = children.Count(child => !child.IsPOINTNull()) + children.Sum(child => child.DescendantCountWithPoint);
                sum += (IsPOINTNull() ? 0 : POINT);
                return sum;
            }

            public double Average => Sum / (DescendantCountWithPoint + (IsPOINTNull() ? 0 : 1));

            public IEnumerable<ITreeListNodeModel> GetChildren()
            {
                return GetAvgTreeRows();
            }
        }

        partial class AvgTreeDataTable
        {
            public AvgTreeRow AddAvgTreeRow(AvgTreeRow parentAvgTreeRowByFkParentReference, string name, double? point)
            {
                if (point.HasValue && !double.IsNaN(point.Value))
                {
                    return AddAvgTreeRow(parentAvgTreeRowByFkParentReference, name, point.Value);
                }
                var row = AddAvgTreeRow(parentAvgTreeRowByFkParentReference, name, -1);
                row.SetPOINTNull();
                return row;
            }
        }

    }
}

namespace AvgTree.DataAccess.AvgTreeDataSetTableAdapters
{
    partial class AvgTreeTableAdapter : IAvgTreeTableAdapter
    {

    }
}

