﻿using System;
using System.Windows.Forms;
using AvgTree.Model.DialogModels;

namespace AvgTree.Visualization
{
    /// <summary>
    /// Use for tree node handler dialogs
    /// </summary>
    internal interface ITreeItemDialog
    {
        /// <summary>
        /// Data binding model
        /// </summary>
        IDlgModelTreeNode TreeItemModel { get; }

        /// <summary>
        /// Notify the caller if the model changes
        /// </summary>
        event EventHandler<DlgModelChangedEventArgs> OnModelChanged;

        /// <summary>
        /// Shows the form as a modal dialog box with the specified owner.
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        DialogResult ShowDialog(IWin32Window owner);
    }
}
