﻿using System;
using System.Windows.Forms;
using AvgTree.Model.DialogModels;

namespace AvgTree.Visualization
{
    internal partial class DeleteITreeListItemModelDlg : Form, ITreeItemDialog
    {
        public readonly DlgModelDeleteTreeNode TheTreeItemModel = new DlgModelDeleteTreeNode();
        public IDlgModelTreeNode TreeItemModel => TheTreeItemModel;
        public event EventHandler<DlgModelChangedEventArgs> OnModelChanged;

        public DeleteITreeListItemModelDlg()
        {
            InitializeComponent();
        }

        private void UpdateITreeListItemModelDlg_Load(object sender, EventArgs e)
        {
            treeItemModelBindingSource.DataSource = TheTreeItemModel;
        }

        private void buttonDeleteNode_Click(object sender, EventArgs e)
        {
            TheTreeItemModel.DeleteChildren = false;
            OnModelChanged?.Invoke(this, new DlgModelChangedEventArgs { Model = TheTreeItemModel });
            DialogResult = DialogResult.Yes;
        }

        private void buttonDeleteWithChildren_Click(object sender, EventArgs e)
        {
            TheTreeItemModel.DeleteChildren = true;
            OnModelChanged?.Invoke(this, new DlgModelChangedEventArgs { Model = TheTreeItemModel });
            DialogResult = DialogResult.Yes;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void textBoxName_Validated(object sender, EventArgs e)
        {
            TheTreeItemModel.Name = textBoxName.Text;
        }
    }
}
