﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using AvgTree.DataAccess;
using AvgTree.Model;
using AvgTree.Model.DialogModels;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;

namespace AvgTree.Visualization
{
    public partial class MainForm : Form
    {
        private readonly ITreeDataManager _treeDataManager = AvgTreeDataManager.Instance;

        public MainForm()
        {
            InitializeComponent();
            _bindingSource.DataSource = TreeListModel.Instance;
            InitRightPanel();
        }

        private void InitRightPanel()
        {
            SuspendLayout();

            var n = 0;
            foreach (var property in typeof(ITreeListNodeModel).GetProperties())
            {
                var y = ++n * 22;
                var label = new Label
                {
                    Location = new Point(0, y),
                    Name = "label" + property.Name,
                    Text = property.Name + @":",
                    TextAlign = ContentAlignment.TopRight,
                    Size = new Size(150, 20)
                };
                splitContainer2.Panel2.Controls.Add(label);

                var textBox = new TextBox
                {
                    Enabled = false,
                    Location = new Point(151, y - 3),
                    Name = "textBox" + property.Name,
                    Size = new Size(100, 20)
                };
                textBox.DataBindings.Add(new Binding("Text", _bindingSource, property.Name, true));
                splitContainer2.Panel2.Controls.Add(textBox);
            }

            ResumeLayout();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _treeDataManager.OnDbChanging += OnDbChanging;
            _treeDataManager.OnDbChanged += OnDbChanged;
            _treeDataManager.OnException += AvgTreeDataManager_OnError;
            _treeDataManager.LoadTreeFromDatabase();
            treeList.OptionsDragAndDrop.DragNodesMode = DragNodesMode.Single;
        }

        private void AvgTreeDataManager_OnError(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = e.ExceptionObject as Exception;
            if (exception != null) MessageBox.Show(this, e.ExceptionObject.ToString(), exception.Message);
        }

        private void OnDbChanging(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(OnDbChanging), sender, e);
            }
            else
            {
                labelDbCommunication.Visible = true;
            }
        }

        private void OnDbChanged(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(OnDbChanged), sender, e);
            }
            else
            {
                _bindingSource.ResetBindings(false);
                labelDbCommunication.Visible = false;
            }
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            showTreeItemHandlerDialog(new NewITreeListItemModelDlg());
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            showTreeItemHandlerDialog(new UpdateITreeListItemModelDlg());
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            showTreeItemHandlerDialog(new DeleteITreeListItemModelDlg());
        }

        private void showTreeItemHandlerDialog(ITreeItemDialog dlg)
        {
            var model = _bindingSource.Current as ITreeListNodeModel;
            dlg.TreeItemModel.Update(model);
            dlg.OnModelChanged += Dlg_OnModelChanged;
            dlg.ShowDialog(this);
            dlg.OnModelChanged -= Dlg_OnModelChanged;
        }

        private void Dlg_OnModelChanged(object sender, DlgModelChangedEventArgs e)
        {
            if (e.Model is DlgModelUpdateTreeNode)
            {
                var model = _bindingSource.Current as ITreeListNodeModel;
                if ((e.Model.Name == model?.Name) && (Equals(e.Model.Point, model?.Point)) && (e.Model.IdParent == model?.IdParent)) return;

                _treeDataManager.Update(model, e.Model.IdParent, e.Model.Name, e.Model.Point);
                e.Model.Update(model);
            }
            else if (e.Model is DlgModelNewTreeNode)
            {
                _treeDataManager.Add(e.Model.IdParent, e.Model.Name, e.Model.Point);
            }
            else if (e.Model is DlgModelDeleteTreeNode)
            {
                var model = _bindingSource.Current as ITreeListNodeModel;
                if (((DlgModelDeleteTreeNode)e.Model).DeleteChildren)
                {
                    dataGridViewChildren.DataSource = null;
                    _treeDataManager.DeleteWithDescendants(model);
                }
                else
                {
                    dataGridViewChildren.DataSource = null;
                    _treeDataManager.Delete(model);
                }
            }
        }

        private void _bindingSource_CurrentChanged(object sender, EventArgs e)
        {
            var model = _bindingSource.Current as ITreeListNodeModel;
            dataGridViewChildren.DataSource = model?.GetChildren();
        }

        private DragDropEffects GetDragDropEffect(TreeList tl, TreeListNode dragNode)
        {
            Point p = tl.PointToClient(MousePosition);
            var targetNode = tl.CalcHitInfo(p).Node;

            if (dragNode != null && dragNode != targetNode)
                return DragDropEffects.Move;
            else
                return DragDropEffects.None;
        }

        private void treeList_DragOver(object sender, DragEventArgs e)
        {
            var dragNode = e.Data.GetData(typeof(TreeListNode)) as TreeListNode;
            e.Effect = GetDragDropEffect(sender as TreeList, dragNode);
        }

        private void treeList_DragDrop(object sender, DragEventArgs e)
        {
            var tl = sender as TreeList;
            Debug.Assert(tl != null, "tl != null");
            var p = tl.PointToClient(new Point(e.X, e.Y));

            var dragNode = e.Data.GetData(typeof(TreeListNode)) as TreeListNode;
            var targetNode = tl.CalcHitInfo(p).Node;

            var dragModel = GetNthModel(dragNode?.Id ?? -1);
            var targetModel = GetNthModel(targetNode?.Id ?? -1);
            _treeDataManager.Update(dragModel, targetModel?.Id, dragModel.Name, dragModel.Point);

            //tl.SetNodeIndex(dragNode, tl.GetNodeIndex(targetNode));
            e.Effect = DragDropEffects.None;
        }

        private static ITreeListNodeModel GetNthModel(int n)
        {
            if (n < 0 || n > TreeListModel.Instance.Nodes.Count - 1) return null;
            return TreeListModel.Instance.Nodes[n];
        }

        private void treeList_CalcNodeDragImageIndex(object sender, CalcNodeDragImageIndexEventArgs e)
        {
            var tl = sender as TreeList;
            if (GetDragDropEffect(tl, tl?.FocusedNode) == DragDropEffects.None)
                e.ImageIndex = -1;  // no icon
            else
                e.ImageIndex = 1;  // the reorder icon (a curved arrow)
        }
    }
}
