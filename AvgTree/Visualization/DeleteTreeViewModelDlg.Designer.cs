﻿using AvgTree.Model.DialogModels;

namespace AvgTree.Visualization
{
    partial class DeleteITreeListItemModelDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.treeItemModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelName = new System.Windows.Forms.Label();
            this.buttonDeleteNode = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonDeleteWithDescendants = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.treeItemModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxName
            // 
            this.textBoxName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.treeItemModelBindingSource, "Name", true));
            this.textBoxName.Location = new System.Drawing.Point(55, 2);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(183, 20);
            this.textBoxName.TabIndex = 0;
            this.textBoxName.Validated += new System.EventHandler(this.textBoxName_Validated);
            // 
            // treeItemModelBindingSource
            // 
            this.treeItemModelBindingSource.DataSource = typeof(AvgTree.Model.DialogModels.DlgModelUpdateTreeNode);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(7, 5);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Name";
            // 
            // buttonDeleteNode
            // 
            this.buttonDeleteNode.Location = new System.Drawing.Point(10, 28);
            this.buttonDeleteNode.Name = "buttonDeleteNode";
            this.buttonDeleteNode.Size = new System.Drawing.Size(156, 23);
            this.buttonDeleteNode.TabIndex = 2;
            this.buttonDeleteNode.Text = "Delete this item only";
            this.buttonDeleteNode.UseVisualStyleBackColor = true;
            this.buttonDeleteNode.Click += new System.EventHandler(this.buttonDeleteNode_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(172, 43);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonDeleteWithDescendants
            // 
            this.buttonDeleteWithDescendants.Location = new System.Drawing.Point(12, 57);
            this.buttonDeleteWithDescendants.Name = "buttonDeleteWithDescendants";
            this.buttonDeleteWithDescendants.Size = new System.Drawing.Size(154, 23);
            this.buttonDeleteWithDescendants.TabIndex = 2;
            this.buttonDeleteWithDescendants.Text = "Delete with descendants";
            this.buttonDeleteWithDescendants.UseVisualStyleBackColor = true;
            this.buttonDeleteWithDescendants.Click += new System.EventHandler(this.buttonDeleteWithChildren_Click);
            // 
            // DeleteITreeListItemModelDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(254, 85);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonDeleteWithDescendants);
            this.Controls.Add(this.buttonDeleteNode);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DeleteITreeListItemModelDlg";
            this.Text = "Delete";
            this.Load += new System.EventHandler(this.UpdateITreeListItemModelDlg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeItemModelBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonDeleteNode;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.BindingSource treeItemModelBindingSource;
        private System.Windows.Forms.Button buttonDeleteWithDescendants;
    }
}