﻿using AvgTree.Model.DialogModels;

namespace AvgTree.Visualization
{
    partial class UpdateITreeListItemModelDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.treeItemModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxPoint = new System.Windows.Forms.TextBox();
            this.labelPoint = new System.Windows.Forms.Label();
            this.labelParentId = new System.Windows.Forms.Label();
            this.textBoxIdParent = new System.Windows.Forms.TextBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.checkBoxDynamicUpdate = new System.Windows.Forms.CheckBox();
            this.comboBoxItems = new System.Windows.Forms.ComboBox();
            this.checkBoxPoint = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.treeItemModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxName
            // 
            this.textBoxName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.treeItemModelBindingSource, "Name", true));
            this.textBoxName.Location = new System.Drawing.Point(55, 2);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(228, 20);
            this.textBoxName.TabIndex = 0;
            this.textBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxName_KeyPress);
            this.textBoxName.Validated += new System.EventHandler(this.textBoxName_Validated);
            // 
            // treeItemModelBindingSource
            // 
            this.treeItemModelBindingSource.DataSource = typeof(DlgModelUpdateTreeNode);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(7, 5);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Name";
            // 
            // textBoxPoint
            // 
            this.textBoxPoint.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.treeItemModelBindingSource, "Point", true));
            this.textBoxPoint.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", this.treeItemModelBindingSource, "PointHasValue", true));
            this.textBoxPoint.Location = new System.Drawing.Point(55, 28);
            this.textBoxPoint.Name = "textBoxPoint";
            this.textBoxPoint.Size = new System.Drawing.Size(178, 20);
            this.textBoxPoint.TabIndex = 0;
            this.textBoxPoint.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPoint_KeyPress);
            this.textBoxPoint.Validated += new System.EventHandler(this.textBoxPoint_Validated);
            // 
            // labelPoint
            // 
            this.labelPoint.AutoSize = true;
            this.labelPoint.Location = new System.Drawing.Point(7, 31);
            this.labelPoint.Name = "labelPoint";
            this.labelPoint.Size = new System.Drawing.Size(31, 13);
            this.labelPoint.TabIndex = 1;
            this.labelPoint.Text = "Point";
            // 
            // labelParentId
            // 
            this.labelParentId.AutoSize = true;
            this.labelParentId.Location = new System.Drawing.Point(7, 57);
            this.labelParentId.Name = "labelParentId";
            this.labelParentId.Size = new System.Drawing.Size(47, 13);
            this.labelParentId.TabIndex = 1;
            this.labelParentId.Text = "ParentId";
            // 
            // textBoxIdParent
            // 
            this.textBoxIdParent.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.treeItemModelBindingSource, "IdParent", true));
            this.textBoxIdParent.Location = new System.Drawing.Point(55, 54);
            this.textBoxIdParent.Name = "textBoxIdParent";
            this.textBoxIdParent.Size = new System.Drawing.Size(228, 20);
            this.textBoxIdParent.TabIndex = 0;
            this.textBoxIdParent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxIdParent_KeyPress);
            this.textBoxIdParent.Validated += new System.EventHandler(this.textBoxIdParent_Validated);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", this.treeItemModelBindingSource, "StaticUpdate", true));
            this.buttonUpdate.Location = new System.Drawing.Point(119, 76);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(200, 76);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // checkBoxDynamicUpdate
            // 
            this.checkBoxDynamicUpdate.AutoSize = true;
            this.checkBoxDynamicUpdate.Checked = true;
            this.checkBoxDynamicUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDynamicUpdate.Location = new System.Drawing.Point(10, 80);
            this.checkBoxDynamicUpdate.Name = "checkBoxDynamicUpdate";
            this.checkBoxDynamicUpdate.Size = new System.Drawing.Size(103, 17);
            this.checkBoxDynamicUpdate.TabIndex = 3;
            this.checkBoxDynamicUpdate.Text = "Dynamic update";
            this.checkBoxDynamicUpdate.UseVisualStyleBackColor = true;
            this.checkBoxDynamicUpdate.CheckedChanged += new System.EventHandler(this.checkBoxDynamicUpdate_CheckedChanged);
            // 
            // comboBoxItems
            // 
            this.comboBoxItems.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.treeItemModelBindingSource, "IdParent", true));
            this.comboBoxItems.FormattingEnabled = true;
            this.comboBoxItems.Location = new System.Drawing.Point(55, 54);
            this.comboBoxItems.Name = "comboBoxItems";
            this.comboBoxItems.Size = new System.Drawing.Size(228, 21);
            this.comboBoxItems.TabIndex = 4;
            // 
            // checkBoxPoint
            // 
            this.checkBoxPoint.AutoSize = true;
            this.checkBoxPoint.Checked = true;
            this.checkBoxPoint.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPoint.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.treeItemModelBindingSource, "PointIsNull", true));
            this.checkBoxPoint.Location = new System.Drawing.Point(239, 30);
            this.checkBoxPoint.Name = "checkBoxPoint";
            this.checkBoxPoint.Size = new System.Drawing.Size(44, 17);
            this.checkBoxPoint.TabIndex = 3;
            this.checkBoxPoint.Text = "Null";
            this.checkBoxPoint.UseVisualStyleBackColor = true;
            // 
            // UpdateITreeListItemModelDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(288, 107);
            this.Controls.Add(this.comboBoxItems);
            this.Controls.Add(this.checkBoxPoint);
            this.Controls.Add(this.checkBoxDynamicUpdate);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.labelParentId);
            this.Controls.Add(this.labelPoint);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxIdParent);
            this.Controls.Add(this.textBoxPoint);
            this.Controls.Add(this.textBoxName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateITreeListItemModelDlg";
            this.Text = "Update";
            this.Load += new System.EventHandler(this.UpdateITreeListItemModelDlg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeItemModelBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxPoint;
        private System.Windows.Forms.Label labelPoint;
        private System.Windows.Forms.Label labelParentId;
        private System.Windows.Forms.TextBox textBoxIdParent;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.BindingSource treeItemModelBindingSource;
        private System.Windows.Forms.CheckBox checkBoxDynamicUpdate;
        private System.Windows.Forms.ComboBox comboBoxItems;
        private System.Windows.Forms.CheckBox checkBoxPoint;
    }
}