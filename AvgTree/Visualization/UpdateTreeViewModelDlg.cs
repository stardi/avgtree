﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AvgTree.Model.DialogModels;

namespace AvgTree.Visualization
{
    internal partial class UpdateITreeListItemModelDlg : Form, ITreeItemDialog
    {
        public readonly DlgModelUpdateTreeNode TheTreeItemModel = new DlgModelUpdateTreeNode();
        public IDlgModelTreeNode TreeItemModel => TheTreeItemModel;
        public event EventHandler<DlgModelChangedEventArgs> OnModelChanged;

        public UpdateITreeListItemModelDlg()
        {
            InitializeComponent();
        }

        private void UpdateITreeListItemModelDlg_Load(object sender, EventArgs e)
        {
            treeItemModelBindingSource.DataSource = TheTreeItemModel;
            checkBoxPoint.CheckedChanged += checkBoxPoint_CheckedChanged;

            if (TheTreeItemModel.Items.Count > 0)
            {
                comboBoxItems.DataSource = new BindingSource {DataSource = TheTreeItemModel.Items};
                comboBoxItems.ValueMember = "Key";
                comboBoxItems.DisplayMember = "Value";
                comboBoxItems.SelectedItem = TheTreeItemModel.Items.SingleOrDefault(item => item.Key == TheTreeItemModel.IdParent);
                comboBoxItems.SelectedIndexChanged += comboBoxItems_SelectedIndexChanged;
                textBoxIdParent.Visible = false;
            }
            else
            {
                comboBoxItems.Visible = false;
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            OnOnModelChanged(false);
            DialogResult = DialogResult.Yes;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected void OnOnModelChanged(bool dynamicUpdate)
        {
            if (dynamicUpdate == TheTreeItemModel.DynamicUpdate)
            {
                OnModelChanged?.Invoke(this, new DlgModelChangedEventArgs { Model = TheTreeItemModel });
            }
        }

        private void checkBoxDynamicUpdate_CheckedChanged(object sender, EventArgs e)
        {
            TheTreeItemModel.DynamicUpdate = checkBoxDynamicUpdate.Checked;
        }

        private void checkBoxPoint_CheckedChanged(object sender, EventArgs e)
        {
            TheTreeItemModel.PointIsNull = checkBoxPoint.Checked;
            OnOnModelChanged(true);
        }

        private void textBoxPoint_Validated(object sender, EventArgs e)
        {
            double point;
            if (double.TryParse(textBoxPoint.Text, out point))
            {
                TheTreeItemModel.Point = point;
            }
            else
            {
                TheTreeItemModel.Point = null;
            }
            OnOnModelChanged(true);
        }

        private void textBoxName_Validated(object sender, EventArgs e)
        {
            TheTreeItemModel.Name = textBoxName.Text;
            OnOnModelChanged(true);
        }

        private void textBoxIdParent_Validated(object sender, EventArgs e)
        {
            int idParent;
            if (int.TryParse(textBoxIdParent.Text, out idParent))
            {
                TheTreeItemModel.IdParent = idParent;
            }
            else
            {
                TheTreeItemModel.IdParent = null;
            }
            OnOnModelChanged(true);
        }

        private void comboBoxItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxItems.SelectedItem != null)
            {
                var kvp = (KeyValuePair<int, string>)comboBoxItems.SelectedItem;
                TheTreeItemModel.IdParent = kvp.Key;
            }
            else
            {
                TheTreeItemModel.IdParent = null;
            }
            OnOnModelChanged(true);
        }

        private void textBoxPoint_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                textBoxPoint_Validated(sender, e);
            }
        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                textBoxName_Validated(sender, e);
            }
        }

        private void textBoxIdParent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                textBoxIdParent_Validated(sender, e);
            }
        }
    }
}
