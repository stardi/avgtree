﻿namespace AvgTree.Visualization
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonNew = new System.Windows.Forms.Button();
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelDbCommunication = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeList = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colCalculatedValue = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colPoint = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colAverage = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colSum = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDescendantCount = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colDescendantCountWithPoint = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridViewChildren = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idParentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.parentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pointDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.averageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calculatedValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.treeListModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChildren)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonNew
            // 
            this.buttonNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNew.Location = new System.Drawing.Point(2, 10);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(75, 23);
            this.buttonNew.TabIndex = 1;
            this.buttonNew.Text = "New";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "Nodes";
            this._bindingSource.DataSource = typeof(AvgTree.Model.TreeListModel);
            this._bindingSource.CurrentChanged += new System.EventHandler(this._bindingSource_CurrentChanged);
            // 
            // labelDbCommunication
            // 
            this.labelDbCommunication.AutoSize = true;
            this.labelDbCommunication.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDbCommunication.ForeColor = System.Drawing.Color.Maroon;
            this.labelDbCommunication.Location = new System.Drawing.Point(1, 66);
            this.labelDbCommunication.Name = "labelDbCommunication";
            this.labelDbCommunication.Size = new System.Drawing.Size(552, 55);
            this.labelDbCommunication.TabIndex = 3;
            this.labelDbCommunication.Text = "Communicating with Db";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUpdate.Location = new System.Drawing.Point(99, 10);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 4;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.Location = new System.Drawing.Point(192, 10);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 5;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeList);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(552, 443);
            this.splitContainer1.SplitterDistance = 273;
            this.splitContainer1.TabIndex = 7;
            // 
            // treeList
            // 
            this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName,
            this.colCalculatedValue,
            this.colId,
            this.colPoint,
            this.colAverage,
            this.colSum,
            this.colDescendantCount,
            this.colDescendantCountWithPoint});
            this.treeList.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeList.DataSource = this._bindingSource;
            this.treeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList.KeyFieldName = "Id";
            this.treeList.Location = new System.Drawing.Point(0, 0);
            this.treeList.Name = "treeList";
            this.treeList.ParentFieldName = "IdParent";
            this.treeList.Size = new System.Drawing.Size(273, 443);
            this.treeList.TabIndex = 0;
            this.treeList.CalcNodeDragImageIndex += new DevExpress.XtraTreeList.CalcNodeDragImageIndexEventHandler(this.treeList_CalcNodeDragImageIndex);
            this.treeList.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeList_DragDrop);
            this.treeList.DragOver += new System.Windows.Forms.DragEventHandler(this.treeList_DragOver);
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colCalculatedValue
            // 
            this.colCalculatedValue.Caption = "Value";
            this.colCalculatedValue.FieldName = "CalculatedValue";
            this.colCalculatedValue.Format.FormatString = "0.###";
            this.colCalculatedValue.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCalculatedValue.Name = "colCalculatedValue";
            this.colCalculatedValue.OptionsColumn.AllowEdit = false;
            this.colCalculatedValue.OptionsColumn.ReadOnly = true;
            this.colCalculatedValue.Visible = true;
            this.colCalculatedValue.VisibleIndex = 1;
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colPoint
            // 
            this.colPoint.FieldName = "Point";
            this.colPoint.Name = "colPoint";
            this.colPoint.OptionsColumn.ReadOnly = true;
            // 
            // colAverage
            // 
            this.colAverage.FieldName = "Average";
            this.colAverage.Name = "colAverage";
            this.colAverage.OptionsColumn.ReadOnly = true;
            // 
            // colSum
            // 
            this.colSum.FieldName = "Sum";
            this.colSum.Name = "colSum";
            this.colSum.OptionsColumn.ReadOnly = true;
            // 
            // colDescendantCount
            // 
            this.colDescendantCount.FieldName = "DescendantCount";
            this.colDescendantCount.Name = "colDescendantCount";
            this.colDescendantCount.OptionsColumn.ReadOnly = true;
            // 
            // colDescendantCountWithPoint
            // 
            this.colDescendantCountWithPoint.FieldName = "DescendantCountWithPoint";
            this.colDescendantCountWithPoint.Name = "colDescendantCountWithPoint";
            this.colDescendantCountWithPoint.OptionsColumn.ReadOnly = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridViewChildren);
            this.splitContainer2.Panel1.Controls.Add(this.buttonDelete);
            this.splitContainer2.Panel1.Controls.Add(this.buttonNew);
            this.splitContainer2.Panel1.Controls.Add(this.buttonUpdate);
            this.splitContainer2.Size = new System.Drawing.Size(275, 443);
            this.splitContainer2.SplitterDistance = 168;
            this.splitContainer2.TabIndex = 7;
            // 
            // dataGridViewChildren
            // 
            this.dataGridViewChildren.AllowUserToAddRows = false;
            this.dataGridViewChildren.AllowUserToDeleteRows = false;
            this.dataGridViewChildren.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewChildren.AutoGenerateColumns = false;
            this.dataGridViewChildren.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewChildren.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.idParentDataGridViewTextBoxColumn,
            this.parentDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.pointDataGridViewTextBoxColumn,
            this.sumDataGridViewTextBoxColumn,
            this.averageDataGridViewTextBoxColumn,
            this.calculatedValueDataGridViewTextBoxColumn});
            this.dataGridViewChildren.DataSource = this.treeListModelBindingSource;
            this.dataGridViewChildren.Location = new System.Drawing.Point(3, 42);
            this.dataGridViewChildren.Name = "dataGridViewChildren";
            this.dataGridViewChildren.ReadOnly = true;
            this.dataGridViewChildren.RowHeadersVisible = false;
            this.dataGridViewChildren.Size = new System.Drawing.Size(264, 123);
            this.dataGridViewChildren.TabIndex = 6;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // idParentDataGridViewTextBoxColumn
            // 
            this.idParentDataGridViewTextBoxColumn.DataPropertyName = "IdParent";
            this.idParentDataGridViewTextBoxColumn.HeaderText = "IdParent";
            this.idParentDataGridViewTextBoxColumn.Name = "idParentDataGridViewTextBoxColumn";
            this.idParentDataGridViewTextBoxColumn.ReadOnly = true;
            this.idParentDataGridViewTextBoxColumn.Visible = false;
            // 
            // parentDataGridViewTextBoxColumn
            // 
            this.parentDataGridViewTextBoxColumn.DataPropertyName = "Parent";
            this.parentDataGridViewTextBoxColumn.HeaderText = "Parent";
            this.parentDataGridViewTextBoxColumn.Name = "parentDataGridViewTextBoxColumn";
            this.parentDataGridViewTextBoxColumn.ReadOnly = true;
            this.parentDataGridViewTextBoxColumn.Visible = false;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pointDataGridViewTextBoxColumn
            // 
            this.pointDataGridViewTextBoxColumn.DataPropertyName = "Point";
            this.pointDataGridViewTextBoxColumn.HeaderText = "Point";
            this.pointDataGridViewTextBoxColumn.Name = "pointDataGridViewTextBoxColumn";
            this.pointDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sumDataGridViewTextBoxColumn
            // 
            this.sumDataGridViewTextBoxColumn.DataPropertyName = "Sum";
            this.sumDataGridViewTextBoxColumn.HeaderText = "Sum";
            this.sumDataGridViewTextBoxColumn.Name = "sumDataGridViewTextBoxColumn";
            this.sumDataGridViewTextBoxColumn.ReadOnly = true;
            this.sumDataGridViewTextBoxColumn.Visible = false;
            // 
            // averageDataGridViewTextBoxColumn
            // 
            this.averageDataGridViewTextBoxColumn.DataPropertyName = "Average";
            this.averageDataGridViewTextBoxColumn.HeaderText = "Average";
            this.averageDataGridViewTextBoxColumn.Name = "averageDataGridViewTextBoxColumn";
            this.averageDataGridViewTextBoxColumn.ReadOnly = true;
            this.averageDataGridViewTextBoxColumn.Visible = false;
            // 
            // calculatedValueDataGridViewTextBoxColumn
            // 
            this.calculatedValueDataGridViewTextBoxColumn.DataPropertyName = "CalculatedValue";
            this.calculatedValueDataGridViewTextBoxColumn.HeaderText = "CalculatedValue";
            this.calculatedValueDataGridViewTextBoxColumn.Name = "calculatedValueDataGridViewTextBoxColumn";
            this.calculatedValueDataGridViewTextBoxColumn.ReadOnly = true;
            this.calculatedValueDataGridViewTextBoxColumn.Visible = false;
            // 
            // treeListModelBindingSource
            // 
            this.treeListModelBindingSource.DataMember = "Nodes";
            this.treeListModelBindingSource.DataSource = typeof(AvgTree.Model.TreeListModel);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 443);
            this.Controls.Add(this.labelDbCommunication);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainForm";
            this.Text = "Average Tree";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChildren)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListModelBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Label labelDbCommunication;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.BindingSource _bindingSource;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraTreeList.TreeList treeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colPoint;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colSum;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDescendantCount;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colDescendantCountWithPoint;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colAverage;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCalculatedValue;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colId;
        private System.Windows.Forms.DataGridView dataGridViewChildren;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idParentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn parentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pointDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn averageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn calculatedValueDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource treeListModelBindingSource;
    }
}

