﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AvgTree.Model.DialogModels;

namespace AvgTree.Visualization
{
    internal partial class NewITreeListItemModelDlg : Form, ITreeItemDialog
    {
        public readonly DlgModelNewTreeNode TheTreeItemModel = new DlgModelNewTreeNode();
        public IDlgModelTreeNode TreeItemModel => TheTreeItemModel;
        public event EventHandler<DlgModelChangedEventArgs> OnModelChanged;

        public NewITreeListItemModelDlg()
        {
            InitializeComponent();
        }

        private void UpdateITreeListItemModelDlg_Load(object sender, EventArgs e)
        {
            treeItemModelBindingSource.DataSource = TheTreeItemModel;
            checkBoxPoint.CheckedChanged += checkBoxPoint_CheckedChanged;

            if (TheTreeItemModel.Items.Count > 0)
            {
                comboBoxItems.DataSource = new BindingSource {DataSource = TheTreeItemModel.Items};
                comboBoxItems.ValueMember = "Key";
                comboBoxItems.DisplayMember = "Value";
                comboBoxItems.SelectedItem = TheTreeItemModel.Items.SingleOrDefault(item => item.Key == TheTreeItemModel.IdParent);
                comboBoxItems.SelectedIndexChanged += comboBoxItems_SelectedIndexChanged;
                textBoxIdParent.Visible = false;
            }
            else
            {
                comboBoxItems.Visible = false;
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            OnModelChanged?.Invoke(this, new DlgModelChangedEventArgs { Model = TheTreeItemModel });
            DialogResult = DialogResult.Yes;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void checkBoxPoint_CheckedChanged(object sender, EventArgs e)
        {
            TheTreeItemModel.PointIsNull = checkBoxPoint.Checked;
        }

        private void textBoxPoint_Validated(object sender, EventArgs e)
        {
            double point;
            if (double.TryParse(textBoxPoint.Text, out point))
            {
                TheTreeItemModel.Point = point;
            }
            else
            {
                TheTreeItemModel.Point = null;
            }
        }

        private void textBoxName_Validated(object sender, EventArgs e)
        {
            TheTreeItemModel.Name = textBoxName.Text;
        }

        private void textBoxIdParent_Validated(object sender, EventArgs e)
        {
            int idParent;
            if (int.TryParse(textBoxIdParent.Text, out idParent))
            {
                TheTreeItemModel.IdParent = idParent;
            }
            else
            {
                TheTreeItemModel.IdParent = null;
            }
        }

        private void comboBoxItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxItems.SelectedItem != null)
            {
                var kvp = (KeyValuePair<int, string>)comboBoxItems.SelectedItem;
                TheTreeItemModel.IdParent = kvp.Key;
            }
            else
            {
                TheTreeItemModel.IdParent = null;
            }
        }
    }
}
