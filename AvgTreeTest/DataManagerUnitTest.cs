﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using AvgTree.DataAccess;
using AvgTree.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvgTreeTest
{
    [TestClass]
    public class DataManagerUnitTest
    {
        private static ITreeDataManager _dataManager;
        private static ITreeDataManager DataManager => _dataManager ?? InitDataManager();

        private static ITreeDataManager InitDataManager()
        {
            var dataManager = AvgTreeDataManager.Instance;
            dataManager.OnDbChanging += DataManager_OnDbChanging;
            dataManager.OnDbChanged += DataManager_OnDbChanged;
            dataManager.OnException += DataManager_OnError;
            dataManager.LoadTreeFromDatabase();
            WaitForDb.WaitOne();
            _dataManager = dataManager;
            Console.WriteLine($@"{TreeListModel.Instance.Nodes.Count} rows loaded from db in {Sw.ElapsedMilliseconds}ms");
            return _dataManager;
        }

        private static void DataManager_OnError(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(@"DataManager_OnError " + e);
        }

        private static readonly Stopwatch Sw = new Stopwatch();
        private static readonly EventWaitHandle WaitForDb = new EventWaitHandle(false, EventResetMode.AutoReset);

        private static void DataManager_OnDbChanging(object sender, EventArgs e)
        {
            Sw.Restart();
        }

        private static void DataManager_OnDbChanged(object sender, EventArgs e)
        {
            Sw.Stop();
            //            Console.WriteLine($@"Db process is executed in {Sw.ElapsedMilliseconds}ms");
            WaitForDb.Set();
        }

        public void Dump()
        {
            foreach (var item in TreeListModel.Instance.Nodes.Where(row => !row.IdParent.HasValue))
            {
                Dump(item);
            }
        }

        private static void Dump(ITreeListNodeModel node, int level = 0)
        {
            if(level==0) Console.WriteLine(@"              Point    Average     Calculated AllChild");
            var dumpString = new string('.', level) + $@"[{node.Id}]";
            dumpString += new string(' ', Math.Max(1, 15 - dumpString.Length)) + $"{node.Point}";
            dumpString += new string(' ', Math.Max(1, 20 - dumpString.Length)) + $"{node.Sum}/{node.DescendantCountWithPoint}={node.Average:#.00}";
            dumpString += new string(' ', Math.Max(1, 37 - dumpString.Length)) + $"{node.CalculatedValue:#.00}";
            dumpString += new string(' ', Math.Max(1, 48 - dumpString.Length)) + $"{node.DescendantCount}";
            Console.WriteLine(dumpString);
            foreach (ITreeListNodeModel r in node.GetChildren())
            {
                Dump(r, level + 1);
            }
        }

        [TestMethod]
        public void DumpDb()
        {
            Dump();
        }

        [TestMethod]
        public void DeleteSingleItem()
        {
            var model = DataManager.Add(null, "Test AddNewItem", null);
            WaitForDb.WaitOne();
            Console.WriteLine($@"New item[{model.Id}] created");
            Dump(model);
            DataManager.Delete(model);
            WaitForDb.WaitOne();
            Console.WriteLine(@"Item is deleted");
        }

        [TestMethod]
        public void DeleteItemKeepChilderen()
        {
            CreateTestTree(100);
            while (_testItems.Count > 0)
            {
                var idx = _rnd.Next(_testItems.Count - 1);
                var model = _testItems[idx];
                DataManager.Delete(model);
                _testItems.Remove(model);
                WaitForDb.WaitOne();
            }
            Console.WriteLine(@"Test items are deleted");
        }

        [TestMethod]
        public void DeleteItemDontKeepChilderen()
        {
            CreateTestTree(100);
            while (_testItems.Count > 0)
            {
                var idx = _rnd.Next(_testItems.Count - 1);
                var model = _testItems[idx];
                DataManager.DeleteWithDescendants(model);
                _testItems.Remove(model);
                WaitForDb.WaitOne();
            }
            Console.WriteLine(@"Test items are deleted");
        }

        private readonly List<ITreeListNodeModel> _testItems = new List<ITreeListNodeModel>();

        private void CreateTestTree(int size)
        {
            ITreeListNodeModel rootModel = DataManager.Add(null, "Test root", null);
            _testItems.Add(rootModel);
            WaitForDb.WaitOne();
            AddTestItems(size);
            Dump(rootModel);
        }

        private readonly Random _rnd = new Random();
        private void AddTestItems(int size)
        {
            while (_testItems.Count < size)
            {
                var parentIndex = _rnd.Next(_testItems.Count - 1);
                var parent = _testItems[parentIndex];
                var point = _rnd.Next(100);

                ITreeListNodeModel model = DataManager.Add(parent.Id, "Test " + _testItems.Count, (point < 30) ? double.NaN : point);
                _testItems.Add(model);
                WaitForDb.WaitOne();
            }
        }
    }
}
